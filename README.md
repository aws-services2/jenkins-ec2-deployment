</details>

******

<details>
<summary>Exercise 1: Create an IAM USER</summary>
 <br />

**steps:**

```sh
# Locally, check/list the subcommands in aws iam
aws iam aa
aws iam create-group --group-name=devops

# List groups
aws iam list-groups

# Check options associated to the create-user subcommand
aws iam create-user aa
aws iam create-user --user-name=*****

# Add newly created user to newly created group
aws iam add-user-to-group --user-name=johni --group-name=devops 

# Login to AWS console as an Admin User and give devops group AmazonVPCFullAccess
# AmazonEC2FullAccess

# Give console access and create access Key for the user, from console

```

</details>

******

<details>
<summary>Exercise 2: Configure AWS CLI </summary>
 <br />

**steps**

```sh
# Set credentials for that user using environment variable
export AWS_ACCESS_KEY_ID="YOUR_ACCESS_KEY_ID"
export AWS_SECRET_ACCESS_KEY= "YOUR_SECRET_ACCESS_KEY"

# Configure correct region for your AWS CLI 
export AWS_DEFAULT_REGION="YOUR_AWS_DEFAULT_REGION" # Or you can use default 

```

</details>

******

<details>
<summary>Exercise 3: Create VPC </summary>
 <br />

**steps**

```sh
# Create a VPC with 1 subnet
# Step 1

# Create a VPC ref http://jodies.de/ipcalc?host=10.0.0.0&mask1=1&mask2=.
aws ec2 create-vpc --cidr-block "10.0.0.0/25" 

# Step 1
# Create a subnet and add it to the vpc
aws ec2 aa # Optional
aws ec2 create-subnet help # Optional
aws ec2 create-subnet --cidr-block "10.0.0.0/25" --vpc-id vpc-0d3a200f7c967a66e

# create a security group in the VPC that will allow you access on ssh port 22 and will allow browser access to your Node application
aws ec2 create-security-group \
--group-name "security-group-developers-server" \
--description "This is a security group for all Devops Engr" \
--vpc-id "vpc-0d3a200f7c967a66e"

aws ec2 authorize-security-group-ingress \
--group-id "sg-0554b984a12d9a6d3" \
--protocol "tcp" --port "22" \
--cidr "**.*.***.**/32" # The IP address that can access the EC2 instance

```

</details>

******

<details>
<summary>Exercise 4: Create EC2 Instance </summary>
 <br />

**steps**

```sh
# Create an EC2 instance in that VPC with the security group you just created and ssh key file
# Step 1

# Create an ssh Keyfile (key pair)
aws ec2 create-key-pair \
--key-name "DEVOPS-KEY" \
--query "KeyMaterial" \
--output text > "DEVOPS-KEY.pem"

# Step 2
# Create an ec2 instance using the Key-pair
aws ec2 run-instances --image-id ami-0766b4b472db7e3b9 \
--count 1 --instance-type t2.micro \
--key-name DEVOPS-KEY \
--security-group-ids sg-0554b984a12d9a6d3 \
--subnet-id subnet-00a65ce32a9bd7632

```

</details>

******

<details>
<summary>Exercise 5: SSH into the server and install Docker on it </summary>
 <br />

**steps**

```sh
# ssh into the server and Install dosker
ssh ec2-user@xx.xxx.xx.x
sudo yum update
sudo yum install docker

# Start docker 
sudo service docker start

# Check docker is running
ps aux | grep docker

# Add current user to the docker group to prevent user from always using sudo
sudo usermod -aG docker $USER
groups # Check user is added
# Logout & in to see the user has been added to the docker group

```

</details>

******

<details>
<summary>Exercise 6: Add docker-compose for deployment </summary>
 <br />

**steps**

```sh
# Add docker-compose file
touch docker-compose.yaml
version: '3'
services:
  my-app:
    image: ${dockerimage}:${PACKAGE_VERSION}
    ports:
      - 3000:3000


```

</details>

******

<details>
<summary>Exercise 7: Add "deploy to EC2 step to your existing pipeline" </summary>
 <br />

**steps**

```sh
# Add a deployment step to the jenkinsfile fom previous exercises shared library project
# https://gitlab.com/build-automation-with-jenkins/jenkins-shared-library

# Also update the application to use the shared library
touch Jenkinsfile


```

</details>

******

<details>
<summary>Exercise 8: Configure access from browser (EC2 Security Group) </summary>
 <br />

**steps**

```sh
# Configure the EC2 security group to access your application from a browser
aws ec2 authorize-security-group-ingress \
--group-id "sg-0554b984a12d9a6d3" \
--protocol "tcp" --port "3000" \
--cidr "0.0.0.0/0" # The IP address that can access the EC2 instance

# Add a docker file 
touch Dockerfile

# Give jenkins access to my EC2 instance
aws ec2 authorize-security-group-ingress \
--group-id "sg-0554b984a12d9a6d3" \
--protocol "tcp" --port "22" \
--cidr "**.*.***.**/32" # The IP address that can access the EC2 instance



```

</details>

******

<details>
<summary>Exercise 9: Configure automatic triggering of multi-branch pipeline </summary>
 <br />

**steps**

```sh
# Add branch based logic to Jenkinsfile


# Add webhook to trigger pipeline automatically
# Add a build strategy using Ignore Committer Strategy plugin
# End



```

</details>




